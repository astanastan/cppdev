#include <iostream>
#include <optional>
#include <string>
using namespace std;

class complex {
  double re, im;
 public:
  complex(double r, double i) : re{r}, im{i} {}
  complex(double r) : re{r}, im{0} {}
  complex() : re{0}, im{0} {}
  double real() const { return re; }
  void real(double d) { re = d; }
  double imag() const { return im; }
  void imag(double d) { im = d; }

  complex &operator+=(complex z) {
    re += z.re;
    im += z.im;
    return *this;
  }
  complex &operator-=(complex z) {
    re -= z.re;
    im -= z.im;
    return *this;
  }
};

optional<string> complex_check(complex a, complex b) {
  if (a.real()==b.real()) return "complex's are equal";
  else return {};

}

void complex_use(int r, int i) {
  complex a(r, i);
  complex b(r);
  a += b;
  cout << "Complex:" << a.real() << "," << a.imag() << endl;
};

int main() {
  complex_use(12,13);
  complex a(1,2);
  complex b(1,2);
  string word = complex_check(a,b).value_or("false");
  cout << word;

}

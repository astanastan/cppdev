print("Hello, world!") //; не ставится
var a: integer = 1 // переменная
let b = 2 // константа
var optionalDay: String? = "monday" // опциональное значение
func greet(day: String) -> String { //функция
    return "today is \(day)."
}
greet(day: optionalDay)

class human {
    var hair: string
    var height = 0
}

let a = human() // экземпляр

print("The height of human a is \(human.height)")

//имерпативный метод
var evens = [Int]()
for i in 1...10 {
    if i % 2 == 0 {
        evens.append(i)
    }
}
print(evens)

//функциональный метод
evens = Array(1...10).filter { $0 % 2 == 0 }
print(evens)

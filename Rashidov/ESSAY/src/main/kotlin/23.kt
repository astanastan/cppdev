object Program {
    @JvmStatic
    fun main(args: Array<String>) {
        val number_7 = School("220", "middle")
        val number_7m = number_7.square_m
        println(number_7m)
        val number_8 = School(120, "middle")
        val number_8m = number_8.square_m
        println(number_8m)
    }
}

internal class School<T>(val square_m: T, var type: String)

/*
public class Program{

    public static void main(String[] args) {

        School<String> number_7 = new School<String>("220", "middle");
        String number_7m = number_7.get_m();
        System.out.println(number_7m);

        School<String> number_8 = new School<String>("120", "middle");
        String number_8m = number_8.get_m();
        System.out.println(number_8m);
    }
}
class School<T>{

    private T square_m;
    private String type;

    School(T square_m, String type){
        this.square_m = square_m;
        this.type = type;
    }

    public T get_m() { return square_m; }
    public String get_t() { return type; }
    public void setType(String type) { this.type = type; }
}

 */
package main

import (
	"fmt"
)

type Building struct {
	SqaureM int
	Address Address
}

type Address struct {
	Number string
	Street string
	City   string
}

type School struct {
	Type string
	Building
}

func (p *Building) Get_sqaurem() {
	fmt.Println("Building is", p.SqaureM, "square m")
}

func (p *Building) Location() {
	fmt.Println("Building at", p.Address.Number, p.Address.Street, p.Address.City)
}

func (c *School) Get_info(){
	fmt.Println("Type:", c.Type)
	fmt.Println("Location:", c.Address.Number, c.Address.Street, c.Address.City)
}

func main() {
	p := Building{SqaureM: 220}
	p.Address = Address{ Number: "45", Street: "Nazarbaev St" }
	p.Address.City = "Pavlodar"
	p.Get_sqaurem()
	p.Location()
	c := School{Type: "middle"}
	c.Address = Address{ Number: "45", Street: "Nazarbaev St" }
	c.Address.City = "Pavlodar"
	c.Get_info()




}   
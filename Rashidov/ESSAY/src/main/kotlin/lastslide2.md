# Примечания

- Использовать CSS для масштабирования текста в режиме презентации в IDEA;
- Создавать отдельный модуль для запуска Java кода;
- Файлы Java нельзя именовать в виде цифр;
- Использовать стабильные ресурсы в качестве ссылок на медиафайлы;
- Проблемы с запуском Swift кода под Windows.   
Личное пожелание по языку программирования C++ при
возможности выбора между:

- C++
- Rust быстрые программы для запуска на браузерах.
  Низкоуровневый язык, осуществляющий механизмы одалживания,
  и владения объектами. Успешно реализуется в сфере создания
  ядер ОС, и веб-проектов.
- Golang быстрые портируемые программы. Попытка от Google
  заменить C/C++. Быстро компилируемый язык, с простым
  синтаксисом. Доступно две IDE: GoLand от JetBrain, и
  LiteIDE.
- swift продвинутый "C++" от Apple. Соотвественно для работы
  требуется MacOS (
  XCode). Легко читаемый, производительный язык.
- Rust быстрые программы для запуска на браузерах.
  Низкоуровневый язык, осуществляющий механизмы одалживания,
  и владения объектами. Успешно реализуется в сфере создания
  ядер ОС, и веб-проектов.
- Golang быстрые портируемые программы. Попытка от Google
  заменить C/C++. Быстро компилируемый язык, с простым
  синтаксисом. Доступно две IDE: GoLand от JetBrain, и
  LiteIDE.
- swift продвинутый "C++" от Apple. Соотвественно для работы
  требуется MacOS (
  XCode). Легко читаемый, производительный язык.
- kotlin продвинутый "C++" от Google/JetBrains
- java портируемый, упрощенный, надежный "C++"
  Язык, поддерживаемый Java. Синтакисис использует многие
  языки. Оффициальный язык Android.
- java портируемый, упрощенный, надежный "C++". Строгий,
  использующий виртуальную машину язык. Это позволяет
  поддерживать язык на любой платформе. Более низкая
  производительность в сравнений с C++.
- ???

---     

##### Направление в С++:

- qt дизайнер (пока не знаю);
- qt дизайнер (пока не знаю); Разработка графических
  интерфейсов. Используеются стандартные элементы и т.д.
- unreal engine (пока не знаю); Игровой движок, использующий
  C++. Все элементы представляют из себя объекты.
- linux/termux (пока не знаю); Эмулятор терминала и среда
  Linux на Android. Обработка данных с Python.
  Программирование в IDE и т.д.
- IOT/EMBEDDED (пока не знаю); Линейка Windows 10. Работа с
  терминалами, бордами и т.д.
- C++ для android (пока не знаю); Android SDK. Нет
  унифицированного способа.
- ???  
  Игровой движок, использующий C++. Все элементы
  представляют из себя объекты.
- linux/termux (пока не знаю); Эмулятор терминала и среда
  Linux на Android. Обработка данных с Python.
  Программирование в IDE и т.д.
- IOT/EMBEDDED (пока не знаю); Линейка Windows 10. Работа с
  терминалами, бордами и т.д.
- C++ для android (пока не знаю); Android SDK. Нет
  унифицированного способа.
- ???

##### Wiki

— веб-сайт, содержимое которого пользователи могут
самостоятельно изменять с помощью инструментов,
предоставляемых самим сайтом. Форматирование текста и
вставка различных объектов в текст производится с
использованием вики-разметки.  
Особый язык разметки — так называемая вики-разметка, которая
позволяет легко и быстро размечать в тексте структурные
элементы и гиперссылки; форматировать и оформлять отдельные
элементы.  
Некоторые вики могут править все посетители сайта.  
Википедия сейчас является самой большой в мире
энциклопедией. Это самая огромная и всесторонняя коллекция
знаний, которые можно обнаружить на протяжении всей истории
человечества. Учитывая, что в английской Википедии уже более
пяти миллионов статей, Википедия как минимум в 30 раз больше
признанной ранее самой огромной энциклопедии —
«Британники» (
около 65 000 статей).

##### Library Genesis (или LibGen)

— веб-сайт, поисковая система и онлайн-хранилище,
предоставляющее бесплатный доступ к книгам и статьям
различной тематики.  
По состоянию на июль 2016 года база данных содержала более
52 миллионов статей из около 50 000 публикаций. По состоянию
на 10 августа 2019 года библиотека Genesis заявляла, что её
база данных содержит почти 2,4 миллиона научно-популярных
книг, 76,4 миллионов статей из научных журналов, 2 миллиона
файлов комиксов, 2,2 миллиона художественных книг, в том
числе 1,3 миллионов на русском языке, 0,38 миллиона номеров
журналов. К 2017 году, по сообщениям, было предоставлено
около 200 миллионов загрузок пользователям со всего мира.
Некоторые издатели, такие как Elsevier, обвиняют Library
Genesis в предоставлении пиратского доступа к статьям и
книгам; в свою очередь, их обвиняют в безнравственном
извлечении выгоды из результатов научных исследований,
финансируемых за счёт налогоплательщиков.

##### CLion

— интегрированная среда разработки для языков
программирования Си и C++, разрабатываемая компанией
JetBrains.  
Состояние CLion — бесплатная пробная версия на 30 дней.
Подходит для операционных систем «Windows», «macOS», и
«Linux». Среди особенностей платформы можно выделить
автозавершение кода, настройка стилей, и анализ кода.
Поддерживает компилятор GCC, Microsoft Visual C++, а также
Clang.  
Работает на основе системы сборки CMake.

##### GIT

– бесплатная система контроля знаний, позволяющая следить за
большими, так и маленькими проектами. Также это -
репозитории, представляющий собой набор утилит командной
строки с параметрами.  
Система спроектирована как набор программ, специально
разработанных с учётом их использования в сценариях. Это
позволяет удобно создавать специализированные системы
контроля версий на базе Git или пользовательские интерфейсы.
Например, Cogito является именно таким примером оболочки к
репозиториям Git, а StGit использует Git для управления
коллекцией исправлений (патчей). Ряд сервисов предоставляют
хостинг для git-репозиториев, среди наиболее известных —
GitHub, Codebase, SourceForge, SourceHut, Gitorious,
Bitbucket, GitLab.  
Гит предоставляет пользователям использовать исходный
контроль знаний. Множество ПО обеспечивает связь с Гит,
осуществляя связь с имеющейся базой.

##### IntelliJ

– интегрированная среда разработки ПО, использующая языки
Java, JavaScrpit, Python.  
Первая версия появилась в январе 2001 года. Быстро приобрела
популярность как первая среда для Java с широким набором
интегрированных инструментов для рефакторинга, которые
позволяли программистам быстро реорганизовывать исходные
тексты программ. Дизайн среды ориентирован на продуктивность
работы программистов, позволяя сконцентрироваться на
функциональных задачах, в то время как IntelliJ IDEA берёт
на себя выполнение рутинных операций.  
Также как и CLion, разрабатывается JetBrains. В большей
степени среда подходит для разработки графического
пользовательского интерфейса. Поддерживает и другие языки,
включая C++. Поддерживает многие языки посредством
подключения плагинов.

##### NAMESPACE

некоторое множество, под которым подразумевается модель,
абстрактное хранилище или окружение, созданное для
логической группировки уникальных идентификаторов (
то есть имён).  
Идентификатор, определённый в пространстве имён,
ассоциируется с этим пространством. Один и тот же
идентификатор может быть независимо определён в нескольких
пространствах. Таким образом, значение, связанное с
идентификатором, определённым в одном пространстве имён,
может иметь (или не иметь) такое же значение, как и такой же
идентификатор, определённый в другом пространстве. Языки с
поддержкой пространств имён определяют правила, указывающие,
к какому пространству имён принадлежит идентификатор (то
есть его определение). В больших базах данных могут
существовать сотни и тысячи идентификаторов. Пространства
имён (или схожие структуры) реализуют механизм для сокрытия
локальных идентификаторов. Их смысл заключается в
группировке логически связанных идентификаторов в
соответствующих пространствах имён, таким образом делая
систему модульной. Ограничение видимости переменных может
также производиться путём задания класса её памяти.   
Языки программирования используют четкие правила, в
соответствии с правилами соответствующего языка.
Пространство имен C++ обычно включает в себя имена типов,
функции, переменные и т.д. Для С++ включение пространства
имеет реализуется через блоком: using namespace
namespace_name;

##### Система типов —

совокупность правил в языках программирования, назначающих
свойства, именуемые типами, различным конструкциям,
составляющим программу — таким как переменные, выражения,
функции или модули.  
Основная роль системы типов заключается в уменьшении числа
багов в программах посредством определения интерфейсов между
различными частями программы и последующей проверки
согласованности взаимодействия этих частей. Эта проверка
может происходить статически или динамически (во время
выполнения), а также быть комбинацией обоих видов.  
Типизация неотъемлимая часть языков программирования,
предотвращающая ошибки несовместимости свойств данных, а
также ошибок адресации. Проверка совместимости типов в
функциях языка C является ярким примером системы типов.

##### Статическая типизация

— приём, широко используемый в языках программирования, при
котором переменная, параметр подпрограммы, возвращаемое
значение функции связывается с типом в момент объявления и
тип не может быть изменён позже
(переменная или параметр будут принимать, а функция —
возвращать значения только этого типа).  
Статическая типизация даёт самый простой машинный код,
поэтому она удобна для языков, дающих исполняемые файлы
операционных систем или JIT-компилируемые промежуточные
коды. Многие ошибки исключаются уже на стадии компиляции,
поэтому статическая типизация хороша для написания сложного,
но быстрого кода. В интегрированной среде разработки
осуществимо более релевантное автодополнение, особенно если
типизация — сильная статическая: множество вариантов можно
отбросить как не подходящие по типу. Чем больше и сложнее
проект, тем большее преимущество даёт статическая типизация,
и наоборот.  
Переменные обычно проверяются на стадии компиляции.
Динамически типизированные языки не могут обрабатывать два
значения разных типов.

##### Динамическая типизация

— приём, используемый в языках программирования и языках
спецификации, при котором переменная связывается с типом в
момент присваивания значения, а не в момент объявления
переменной. Таким образом, в различных участках программы
одна и та же переменная может принимать значения разных
типов. Примеры языков с динамической типизацией — Smalltalk,
Python, Objective-C, Ruby, PHP, Perl, JavaScript, Лисп.  
Динамическая типизация упрощает написание программ для
работы с меняющимся окружением, при работе с данными
переменных типов; при этом отсутствие информации о типе на
этапе компиляции повышает вероятность ошибок в исполняемых
модулях. В некоторых языках со слабой динамической
типизацией стоит проблема сравнения величин, так, например,
PHP имеет операции сравнения «==», «!=» и «===», «!==», где
вторая пара операций сравнивает и значения, и типы
переменных. Операция «===» даёт true только при полном
совпадении, в отличие от «==», который считает верным такое
выражение: (1=="1"), однако эта проблема не динамической
типизации в целом, а конкретных языков программирования.  
При динамической типизации, типы значения не проверяются
вплодь до запуска кода.

##### Явное назначение типов, или явная типизация

— в программировании вид типизации, при котором в момент
объявления переменной требуется явно указать ее тип.
Противоположность явной типизации — неявная типизация, при
которой эта задача перекладывается на транслятор языка.
Например, если переменной X нужно присвоить целое число, для
данной переменной нужно непосредственно при объявлении
указать целочисленный тип.

##### С++ структуры

композитный тип данных, инкапсулирующий без сокрытия набор
значений различных типов. Порядок размещения значений в
памяти задаётся при определении типа и сохраняется на
протяжении времени жизни объектов, что даёт возможность
косвенного доступа (например, через указатели).  
Представляет собой производный тип данных, который
представляет какую-то определенную сущность. В C++ понятие
структуры было расширено до класса, то есть была добавлена
возможность включения в структуру функций-методов. Главное
отличие состоит в том, что в соответствии с «правилом трёх»
структуры всегда имеют конструктор, даже если явное его
определение в исходном коде отсутствует. Таким образом,
говорить о «структурах в С++» некорректно.  
Структуры задаются ключевым словом struct. Для обращения к
элементам структуры используюи точку (.), а для обращения
через указатель - оператор ->.

##### С++ классы

в объектно-ориентированном программировании, представляет
собой шаблон для создания объектов, обеспечивающий начальные
значения состояний:
инициализация полей-переменных и реализация поведения
функций или методов. Другие абстрактные типы данных —
метаклассы, интерфейсы, структуры, перечисления, —
характеризуются какими-то своими, другими особенностями.
Класс является ключевым понятием в ООП. Суть отличия классов
от других абстрактных типов данных состоит в том, что при
задании типа данных класс определяет одновременно как
интерфейс, так и реализацию для всех своих экземпляров (т.е.
объектов).  
Почти каждому члену класса можно установить модификатор
доступа (за исключением статических конструкторов и
некоторых других вещей). В большинстве
объектно-ориентированных языков программирования
поддерживаются следующие модификаторы доступа:

- private;
- public;
- protected.

##### С++ объединения

Объединение (union) представляет собой структуру (struct), в
которой все члены располагаются по одному и тому же адресу,
так что union занимает столько же памяти, сколько и его
наибольший член. Естественно, union может хранить
одновременно значение только одного члена.  
Все члены объединения открыты по умолчанию. Как и в классе,
на отдельные переменные, составляющие объединение, можно
ссылаться с помощью оператора (.). Оператор -> используется
для доступа к объединению с помощью указателя.

##### С++ перечисления

используются для представления небольших множеств
целочисленных значений. Помогают сделать код более
удобночитаемым.  
Объявляются ключевым словом enum. Это пользовательский тип,
поэтому можно определить для него операторы

##### ООП

— методология программирования, основанная на представлении
программы в виде совокупности объектов, каждый из которых
является экземпляром определённого класса, а классы образуют
иерархию наследования. Идеологически ООП — подход к
программированию как к моделированию информационных
объектов, решающий на новом уровне основную задачу
структурного программирования: структурирование информации с
точки зрения управляемости, что существенно улучшает
управляемость самим процессом моделирования, что, в свою
очередь, особенно важно при реализации крупных проектов.  
Определяют основные принципы ООП:

- Инкапсуляция;
- Наследование;
- Полиморфизм.  
  В настоящее время количество прикладных языков
  программирования (список языков), реализующих
  объектно-ориентированную парадигму, является наибольшим по
  отношению к другим парадигмам. Наиболее распространённые в
  промышленности языки (
  C++, Delphi, C#, Java и др.) воплощают объектную модель
  Симулы. Примерами языков, опирающихся на модель Смолтока,
  являются Objective-C, Python, Ruby.

##### Инкапсуляция

размещение в одном компоненте данных и методов, которые с
ними работают.В ООП инкапсуляция тесно связана с принципом
абстракции данных. В сообществе C++ или Java принято
рассматривать инкапсуляцию без сокрытия как неполноценную.  
В общем случае в разных языках программирования термин
«инкапсуляция» относится к одной или обеим одновременно
следующим нотациям:

- механизм языка, позволяющий ограничить доступ одних
  компонентов программы к другим;
- языковая конструкция, позволяющая связать данные с
  методами, предназначенными для обработки этих данных.

В с++ разделяют несколько уровней доступа, такие как
Private, и Public. Так, данные имеют уровень открытости,
ограничевая доступ в пределах уровня.

##### Наследование

концепция объектно-ориентированного программирования,
согласно которой абстрактный тип данных может наследовать
данные и функциональность некоторого существующего типа,
способствуя повторному использованию компонентов
программного обеспечения.  
Наследование является механизмом повторного использования
кода и способствует независимому расширению программного
обеспечения через открытые классы и интерфейсы. Установка
отношения наследования между классами порождает иерархию
классов.

В иерархии классов может быть суперкласс, от коророго
наследовали все остальные классы. Суперклассами могут быть
абстрактные типы, подклассы, интерфейс и базовый класс.
Иерархия наследования или иерархия классов — дерево,
элементами которого являются классы и интерфейсы.

##### Полиморфизм

способность функции обрабатывать данные разных типов.  
Принципиальная возможность для одного и того же кода
обрабатывать данные разных типов определяется свойствами
системы типов языка.С этой точки зрения различают
статическую неполиморфную типизацию
(потомки Алгола и BCPL), динамическую типизацию
(потомки Lisp, Smalltalk, APL) и статическую полиморфную
типизацию
(потомки ML). Использование ad-hoc-полиморфизма наиболее
характерно для неполиморфной типизации. Параметрический
полиморфизм и динамическая типизация намного существеннее,
чем ad-hoc-полиморфизм, повышают коэффициент повторного
использования кода, поскольку определённая единственный раз
функция реализует без дублирования заданное поведение для
бесконечного множества вновь определяемых типов,
удовлетворяющих требуемым в функции условиям. С другой
стороны, временами возникает необходимость обеспечить
различное поведение функции в зависимости от типа параметра,
и тогда необходимым оказывается специальный полиморфизм.
Параметрический полиморфизм является синонимом абстракции
типа. Он повсеместно используется в функциональном
программировании, где он обычно обозначается просто как
«полиморфизм». В сообществе объектно-ориентированного
программирования под термином «полиморфизм» обычно
подразумевают наследование, а использование параметрического
полиморфизма называют обобщённым программированием, или
иногда «статическим полиморфизмом».

##### Абстрактный тип

это тип, который полностью изолирует пользователя от деталей
реализации.  
Формально АТД может быть определён как множество объектов,
определяемое списком компонентов
(операций, применимых к этим объектам, и их свойств). Вся
внутренняя структура такого типа спрятана от разработчика
программного обеспечения — в этом и заключается суть
абстракции. Абстрактный тип данных определяет набор функций,
независимых от конкретной реализации типа, для оперирования
его значениями. Конкретные реализации АТД называются
структурами данных.  
В программировании абстрактные типы данных обычно
представляются в виде интерфейсов, которые скрывают
соответствующие реализации типов. Программисты работают с
абстрактными типами данных исключительно через их
интерфейсы, поскольку реализация может в будущем измениться.
Такой подход соответствует принципу инкапсуляции в
объектно-ориентированном программировании. Сильной стороной
этой методики является именно сокрытие реализации. Раз вовне
опубликован только интерфейс, то пока структура данных
поддерживает этот интерфейс, все программы, работающие с
заданной структурой абстрактным типом данных, будут
продолжать работать.

##### Виртуальная функция

в объектно-ориентированном программировании метод (функция)
класса, который может быть переопределён в
классах-наследниках так, что конкретная реализация метода
для вызова будет определяться во время исполнения. Таким
образом, программисту необязательно знать точный тип объекта
для работы с ним через виртуальные методы: достаточно лишь
знать, что объект принадлежит классу или наследнику класса,
в котором объявлен метод.  
Слово virtual в C++ означает "может быть определен позже в
классе, производимым от данного".  
Виртуальные методы — один из важнейших приёмов реализации
полиморфизма. Они позволяют создавать общий код, который
может работать как с объектами базового класса, так и с
объектами любого его класса-наследника. При этом базовый
класс определяет способ работы с объектами, и любые его
наследники могут предоставлять конкретную реализацию этого
способа.

##### Основные операции

К основным операциям C++ относятся операции копирования,
перемещения, деструкторы, конструктуры и т.д. Мы должны
определять их как соглосованный набор или в противном случае
страдать от логических проблем, или проблем с
производительностью.  
Для избежения этого следует проектировать их как
соглосованный набор операций. По умолчанию следует объявлять
конструктор с единственным аргументом как explicit.  
Операции копирования подходят для небольших данных, но
известны своей надежностью, в свою очередь перемещение
подходит для больших потоков данных. Контейнеры явялются
отличным выбором для операций.  
Некоторые операции при их определении для типа имеют обычный
смысл. Этот обычный смысл часто предполагается
программистами и библиотеками
(в частности, стандартной библиотекой), поэтому разумно
соответствовать ему при разработке новых типов, для которых
эти операции имеют смысл. • Сравнения: ==, !
=, <, <=, > и >=. • Операции с контейнерами: size (),
begin () и end (). • Операции ввода-вывода:>> и<<. •
Пользовательские литералы. • swap (). • Хеш-функции: hash<>.

##### Шаблоны

это класс или функция, которую мы параметризуем с помощью
набора типов или значений. Шаблоны используют для
представления идей, которые лучше всего воспринимаются как
нечто общее, из которого мы можем генерировать конкретные
типы и функции, указывая аргументы.  
С помощью щаблонов можно представить вектор элементов
определенного типа как произвольный:  
template typename T;  
Чаще всего шаблон имеет смысл только для аргументов шаблона,
которые отвечают определенным критериям. В дополнении к
этому шаблоны могут получать значения.  
Шаблоны имеют гораздо больше возможностей, чем простая
параметризация контейнера типом элемента. В частности, они
широко используются для параметризации типов и алгоритмов
стандартной библиотеки.

##### C++

компилируемый, статически типизированный язык
программирования общего назначения. Поддерживает такие
парадигмы программирования, как процедурное
программирование, объектно-ориентированное программирование,
обобщённое программирование. Язык имеет богатую стандартную
библиотеку, которая включает в себя распространённые
контейнеры и алгоритмы, ввод-вывод, регулярные выражения,
поддержку многопоточности и другие возможности. C++ сочетает
свойства как высокоуровневых, так и низкоуровневых языков.

Стандартная бибиотека С++ содержит все средства С, строки,
регулярные выражения, потоки ввода-вывода, контейнеры,
числовые вычесления, паррарельное программирование, утилиты
для поддержки шаблонного программирования, обобщенного
программирования, и общего программирования. указатели,
суффиксы.

##### Ввод и вывод

Бибилиотека потоков ввода-вывода обеспечивает
форматированный и неформатированный буферизованный
ввод-вывод текстовых и числовых значений.

- ostream преобразует типизированные объекты в поток
  символов (байтов).
- istream преобразует поток символов (байтов) в
  типизированные объекты.

У всех классов ввода-вывода есть дестркукторы, которые
освобождают потоки. Стандартный выходной поток называется
cout. В свою очередь стандартный поток сообщения об ошибках
cerr.

Оператор >> используется в качестве оператора ввода; cin
представляет собой стандартный входной поток. Тип правого
операнда оператора>> определяет, какие входные данные
приемлемы и каков целевой объект ввода.

##### Контейнеры

Класс с основной целью хранения объектов обычно называется
контейнером.

Наиболее полезным контейнером стандартной библиотеки
является vector. Типичная реализация vector будет состоять
из дескриптора, хранящего указатель на первый элемент, на
элемент, следующий за последним, и на элемент, следующий за
выделенной памятью  (или эквивалентную информацию,
представленную как указатель плюс смещения).Копирование и
перемещение вектороров реализуется конструкторами и
операторами присваивания. Элементом вектора может быть
некоторый тип, почти любой. Для проверки выхода за границы
диапозона испольюуется функция at(). Она полезна т.к. vector
не реализует это сам.

Также контейнеры могут быть в виде двусвязного списка list.
list используется для последовательностей, в которые
эффективно вставлять (и удалять) элементы, не перемещая при
этом другие элементы. Основное отличие - отсуствие
индексации. Поиск и последующие операции производятся по
поиску значения.

В ином варианте стандартная библиотека предлагает
сбалансированное двоичное дерево поиска map. map известен
как ассоциативный массив или словарь.

##### Алгоритмы

набор конечного числа правил, задающих последовательность
выполнения операций для решения задачи определенного типа
... [и] и.имеет пять важных особенностей: Конечность ."
Определенность ... Ввод ". Вывод ... Эффективность".В
контексте стандартной библиотеки С++ алгоритм является
шаблоном функции, работающим с последовательностями
элементов.

Алгоритмы определены в пространстве имен std и представлены
в заголовочном файле <algorithm>. Эти алгоритмы стандартной
библиотеки принимают в качестве входных данных
последовательности.

Вызов back_
inserter ( res) создает итератор для списка res, который
добавляет элементы в конец контейнера, расширяя последний
так, чтобы для них нашлось место.

Алгоритм find ищет значение в последовательности, и
возвращает итератор найденного элемента.

Предикат ищет элемент, удовлетворяющий некоторым условиям.

---
![](algo.png)

---

##### Утилиты

##### Управление ресурсами

Ресурс - это то, что должно быть захвачено, а позже (явно
или неявно) освобождено. Примерами являются память,
блокировки, сокеты, дескрипторы потоков выполнения и
файловые дескрипторы. Для длительно работающей программы
неспособность своевременно освободить ресурс ("утечка")
может привести к серьезному снижению производительности и,
возможно, даже к аварийному завершению программы.

Компоненты стандартной библиотеки разработаны так, чтобы не
допускать утечки ресурсов. Для этого они полагаются на
базовую языковую поддержку управления ресурсами с
использованием пар "конструктор/деструктор", чтобы
гарантировать, что ресурс не переживет объект, ответственный
за него.

В загаловочном файле <memory> стандартной бибилотеки
предоставлены два 'интелектуальных указателя', которые
управлять объектами в динамической памяти:

- unique _ptr для предоставление единственного владения;
- shared _ptr для предоставления совместного владения.

Функция move() используется для перемещения указателей, т.
к. копирование для вышеназванных 'интелектуальных
указателей' недоступно. В случае если есть желание
перемещения значения из одного указателя в другой, но без
значения "по умолчанию" для отправителя, используется
функция forward().

##### Проверка выхода за границы диапазона

Традиционно ошибки выхода за границы диапазона были основным
источником серьезных ошибок в программах на С и С++.
диапазону значительно уменьшили эту проблему, но можно
сделать больше. Основным источником ошибок выхода за границы
диапазона является то, что люди передают указатели (обычные
или интеллектуальные), а затем, чтобы узнать количество
элементов, на которые они указывают, полагаются на
соглашение.

string_ span представляет собой пару (указатель, длина),
обозначающую последовательность элементов. span
предоставляет доступ к непрерывной последовательности
элементов. Элементы могут храниться разными способами, в том
числе в векторах и встроенных массивах. Как и указатель,
span не владеет символами, на которые указывает.

void fs(span<int> р)  
{  
for (int x : p)  
x=0;  
}

##### Альтернативы

Стандартная библиотека предлагает три типа для выражения
альтернатив:
• variant - для представления одной из определенного
множества альтернатив (заголовочный файл- <variant>);   
• optional - для представления значения определенного типа
или отсутствия значения (заголовочный файл- <optional>);   
• any - для представления одного из неограниченного
множества альтернативных типов (заголовочный файл - <any> ).

Эти три типа предлагают пользователю схожие функциональные
возможности. К сожалению, они не предлагают единого
интерфейса.

##### Аллокаторы

По умолчанию контейнеры стандартной библиотеки выделяют
память с помощью new. Операторы new и delete предоставляют
общую свободную память (также именуемую динамической памятью
или кучей), которая может содержать объекты произвольного
размера и со временем жизни, контролируемым пользователем.
Это влечет определенные затраты времени и памяти, которые
могут быть устранены во многих частных случаях. Поэтому
контейнеры стандартной библиотеки предоставляют возможность
устанавливать распределители памяти (аллокаторы) с
определенной семантикой там, где это необходимо.

Традиционное решение проблем фрагментации - переписать код с
использованием аллокаторов пулов. Аллокатор пула - это
распределитель, который управляет объектами единого
фиксированного размера и выделяет пространство для множества
объектов одновременно, а не использует отдельные выделения.
К счастью, С++ 17 предлагает прямую поддержку этого решения.
Распределитель пула определен в подпространстве имен pmr (
polymorphic memory resource - полиморфный ресурс памяти) в
std.

##### Время

В заголовочном файле std::chrono стандартная библиотека
предоставляет функциональные возможности для работы со
временем.

using namespace std::chroпo;  
auto t0 = high_resolution_clock::now();  
do_work();  
auto t1 = high_resolution_clock::now();  
cout << duration_cast<milliseconds>(t1-t0).count() <<
"мс\n";

##### Адаптация функций

При передаче функции в качестве аргумента функции тип
аргумента должен соответсвовать ожидаемому, выраженному в
объявлении вызываемой функции. В этом случае используются
три альтернативы:

- лямбда выражения;
- std::mem_fn() для создания функционалного объекта;
- определить функцию как принимающую std::function.

##### Функция типов

это функция, которая вычисляется во время компиляции, с
типом в качестве аргумента или возвращающая тип. Стандартная
библиотека предоставляет множество функции типов.

numeric_limits из заголовочного файла limits предоставляют
всяческую полезную информацию для числовых значений(например
min).

fоrward_list предлагает однонаправленные итераторы, которые
можно использовать для обхода последовательности с помощью
алгоритмов и циклов for.

Стандартная библиотека предоставляет механизм
iterator_traits, который позволяет проверять, какой тип
итератора предоставляется

##### Параллельные вычисления

Параллелизм - выполнение нескольких задач одновременно -
широко используется для повышения пропускной способности (
путем использования нескольких процессоров для единого
вычисления) или для снижения времени отклика (позволяя одной
части программы выполняться, в то время как другая ожидает
ответа).

Стандартная библиотека поддерживает средства: thread, mutex,
lock(), packaged_task, и future.Эти средства построены
непосредственно на примитивах, предлагаемых операционными
системами, и не приводят к снижению производительности по
сравнению с их непосредственным применением. Они также не
обещают какого-либо существенного улучшения
производительности по сравнению с тем, что предлагает
операционная система.

##### Thread

Вычисление, которое потенциально может быть выполнено
одновременно с другими вычислениями, называется заданием (
task). Поток (thread) - это системное представление задания
в программе. Задание, которое должно выполняться
одновременно с другими заданиями, запускается на выполнение
путем создания объекта std:: thread (описан в заголовочном
файле
<thread>) с заданием в качестве аргумента. Задание
представляет собой функцию или функциональный объект:  
thread t1 {f}; // f() выполняется в отдельном потоке
t1.join(); // Ожидание потока t1

##### Совместное использование данных

Как обеспечить выполнение условия, чтобы одновременно доступ
к заданному набору объектов имело не более одного задания?
Основополагающим элементом решения этой задачи является
*mutex*
(mutual exclusion object - объект взаимоисключения, мьютекс)
. Поток thread захватывает mutex с помощью операции lock
():  
mutex m; // Управляляющий мьютекс  
scoped_lock lck {m}; // Захват мьютекса 

Конструктор scoped _
lock захватывает мьютекс (вызовом m. lock ()).Если другой
поток уже захватил мьютекс, наш поток ожидает ("
блокируется"), пока другой поток не завершит свой доступ к
данным. Как только другой поток завершит свой доступ к общим
данным, scoped lock освобождает мьютекс (вызовом m.
unlock ()).Когда мьютекс освобождается, потоки, ожидающие
его, возобновляют выполнение ("пробуждаются"). 

scoped_lock - захват нескольких блокировок.
unique_lock - захват одной блокировки.

#####  C++ История

Работа по созданию С++ началась осенью 1979 года, под 
названием "С с классами". В нем отсуствовали шаблоны, 
исключения, перегрузка операторов, ссылки, виртуальные 
функции, исключения и многое другое.
Первое исследование С++ вне исследовательской 
организации началось в июле 1983 года.
Наиболее важной особенностью С++ в то время было 
введение конструкторов и деструкторов. В 1985 году С++ 
был выпущен на продажу.

С++ не является объектно-ориентированным языком, а лишь поддерживает его 
элементы, а также абстракцию данных. Во второй половине 1980-х С++ продолжал 
развиваться. Появились шаблоны, а также обработка исключении. Проектирование 
исключение было сосредоточено в том числе и на управление ресурсами, с 
помощью освобождающих декструкторов. Эту технику назвали неуклюже назвали 
"Захват ресурса есть инициализация" (Resource Acquisition Is Initialization),
с аббривеатурой "RAII".

##### .dll .lib
Статические и динамические бибилотеки.  

lib - библиотека в виде объектных 
файлов подключаемых к исходной программе на этапе компановки. 

dll - библиотека, работающая по запросу выполняемой программы, в момент 
запуска. Проверяется наличие библиотек в операционной системе.

Экземпляр статической бибилотеки может использоваться лишь для одной 
программы. Т.е. эффективно для небольших библиотек. В свою очередь 
экземпляры динамической бибилотеки могут использоватся для разных программ. 
Но могут присутсвовать конфликты версии. 
Динамические бибилотеки представляют из себя исполняемые файлы, тем не менее 
требуются ссылки от исполняемой программы. 

При использовании динамической бибилотеки роль раздатчика исполняет 
операционная система. 








 